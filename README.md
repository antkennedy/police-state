# 🚓🚨Establishing a police state! 🚨🚓

Prometheus / Grafana deployment used along side presentation.

## Getting Started

In order to run this example please make sure you have `docker` and `docker-compose` installed on your local system

```bash
$ docker-compose up
```

The following endpoints are then made available.

- api - http://localhost:8090/
- prometheus - http://localhost:9090/
- grafana - http://localhost:3000/ 

## Grafana setup

- default username/password `admin/admin`
- add data prometheus data source http://prometheus:9090/
- import dashboard from json `config/dashboards/api.json`
