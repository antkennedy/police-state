const prom = require('prom-client');
const collectDefaultMetrics = prom.collectDefaultMetrics;

// Probe every 5th second.
collectDefaultMetrics({ timeout: 5000 });

const pingCounter = new prom.Counter({
    name: 'ping_counter',
    help: 'counter for number of pings'
});

const randCounter = new prom.Counter({
    name: 'random_counter',
    help: 'counter for number of randoms'
});

const randErrorCounter = new prom.Counter({
    name: 'random_error_counter',
    help: 'counter for number of random errors'
});

const httpHist = new prom.Histogram({ 
    name: 'http_request_buckets_milliseconds',
    help: 'request duration buckets in milliseconds. Bucket size set to 500 and 2000 ms',
    labelNames: ['method', 'path', 'status'],
    buckets: [ 500, 2000 ]
});

const httpSum = new prom.Summary({
    name: 'http_request_duration_milliseconds',
    help: 'request duration in milliseconds',
    labelNames: ['method', 'path', 'status']
});

const ms = (start) => {
    var diff = process.hrtime(start)
    return Math.round((diff[0] * 1e9 + diff[1]) / 1000000)
}

const observe = (method, path, statusCode, start) => {
    path = path ? path.toLowerCase() : ''

    const duration = ms(start)
    method = method.toLowerCase();
    httpHist.labels(method, path, statusCode).observe(duration)
    httpSum.labels(method, path, statusCode).observe(duration)
};

module.exports = {
    pingCounter,
    randCounter,
    randErrorCounter,
    httpHist,
    httpSum,
    observe
}