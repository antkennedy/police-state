const metrics = require('./metrics');
const routes = require('./routes');
const express = require('express')

const app = express();
const port = 8090;

// get prometheus metrics
app.get('/metrics', routes.metrics);
// return pong!
app.get('/ping', routes.pong);
// get a random number (0 - 100) with a 
// 20% chance of throwing an error
app.get('/random', routes.random);

app.listen(port, () => console.log(`Listening on port ${port}!`))