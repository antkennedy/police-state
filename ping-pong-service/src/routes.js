const prom = require('prom-client');
const metric = require('./metrics');

const randomInt = (max) => {
    const rand =  Math.floor(Math.random() * Math.floor(max));
    if (rand % 5 === 0) {
        throw new Error('Error creating random number');
    }
    return rand;
}

const metrics = (req, res) => {
    res.set('Content-Type', prom.register.contentType);
    res.end(prom.register.metrics());
};

const pong = (req, res) => {
    const startTime = process.hrtime();
    res.set('Content-Type', 'text/plain');
    res.end('pong!');

    metric.pingCounter.inc();
    metric.observe('GET', '/ping', 200, startTime);
}

const random = (req, res) => {
    const startTime = process.hrtime();
    setTimeout(() => {
        let rand;
        try {
            rand = randomInt(100);
        }
        catch (error) {
            res.status(500);
            res.send('server error');

            metric.randErrorCounter.inc();
            metric.observe('GET', '/random', 500, startTime);
            return;
        }
        
        res.set('Content-Type', 'text/plain');
        res.end(rand.toString());

        metric.randCounter.inc();
        metric.observe('GET', '/random', 200, startTime);
        
    }, Math.random() * 1000);
}

module.exports = {
    metrics,
    pong,
    random,
};
